package com.epayment.services;

import com.epayment.assertj.ProductAssert;
import com.epayment.assertj.ResponseAssert;
import com.epayment.cache.LRUCache;
import com.epayment.interfaces.rest.Product;
import com.epayment.util.MockitoTestInit;
import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(HierarchicalContextRunner.class)
public class RESTProductsServiceTest extends MockitoTestInit {

    private static final Integer ID = 1;
    private static final String NAME = "Product Name";
    private static Product product;

    @InjectMocks
    private RESTProductsService productsService;

    @Mock
    private LRUCache<Integer, Product> lruCache;

    @Before
    public void givenProduct() {
        product = new Product(ID, NAME);
    }

    public class AddProductContext {

        private final URI URI_BASE = URI.create("http://localhost:8080/");
        private final URI EXPECTED_URI = URI.create("http://localhost:8080/product/" + ID);
        private ArgumentCaptor<Product> productCaptor;
        private ArgumentCaptor<Integer> idCaptor;
        private UriComponentsBuilder uriBuilder;

        @Before
        public void initialize() {
            uriBuilder = UriComponentsBuilder.fromUri(URI_BASE);
            productCaptor = ArgumentCaptor.forClass(Product.class);
            idCaptor = ArgumentCaptor.forClass(Integer.class);
        }

        @Test
        public void whenAddProductToCache_thenReturnStatusCodeCreated() {
            ResponseEntity<Void> response = productsService.add(product, uriBuilder);

            ResponseAssert.assertThat(response).hasStatusCodeCreated().hasLocation(EXPECTED_URI);
            verify(lruCache).put(idCaptor.capture(), productCaptor.capture());
            assertThat(idCaptor.getValue()).as("Cache Key").isEqualTo(ID);
            ProductAssert.assertThat(productCaptor.getValue()).hasId(ID).hasName(NAME);
        }
    }

    public class EmptyCacheContext {

        @Before
        public void givenEmptyCache() {
            when(lruCache.get(ID)).thenReturn(Optional.empty());
            when(lruCache.remove(ID)).thenReturn(Optional.empty());
        }

        @Test
        public void whenGetProductById_thenReturnStatusCodeNotFound() {
            ResponseEntity<Product> response = productsService.get(ID);

            ResponseAssert.assertThat(response).hasStatusCodeNotFound();
        }

        @Test
        public void whenDeleteProductById_thenReturnStatusCodeNotFound() {
            ResponseEntity<Product> response = productsService.delete(ID);

            ResponseAssert.assertThat(response).hasStatusCodeNotFound();
        }
    }

    public class NonEmptyCacheContext {

        @Before
        public void givenNonEmptyCache() {
            when(lruCache.get(ID)).thenReturn(Optional.of(product));
            when(lruCache.remove(ID)).thenReturn(Optional.of(product));
        }

        @Test
        public void whenGetProductById_thenReturnProductAndStatusCodeOk() {
            ResponseEntity<Product> response = productsService.get(ID);

            ResponseAssert.assertThat(response).hasStatusCodeOk();
            ProductAssert.assertThat(response.getBody()).hasId(ID).hasName(NAME);
        }

        @Test
        public void whenDeleteProductById_thenReturnStatusCodeNoContent() {
            ResponseEntity<Product> response = productsService.delete(ID);

            ResponseAssert.assertThat(response).hasStatusCodeNoContent();
        }

        @Test
        public void whenInvalidate_thenReturnStatusCodeOk() {
            ResponseEntity<Void> response = productsService.invalidate();

            ResponseAssert.assertThat(response).hasStatusCodeOk();
            verify(lruCache).invalidate();
        }

    }
}
