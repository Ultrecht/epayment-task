package com.epayment.services;

import com.epayment.assertj.ProductAssert;
import com.epayment.assertj.ResponseAssert;
import com.epayment.interfaces.rest.Product;
import com.epayment.util.SpringWebTestInit;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static junitparams.JUnitParamsRunner.$;

@RunWith(JUnitParamsRunner.class)
public class RESTProductServiceIT extends SpringWebTestInit {

    public static final String BASE_URI = "http://localhost:8080/product/";
    public static final String INVALIDATE_CACHE = "cache/contents";


    public static final Integer EXISTING_ID = 1;
    public static final Integer NON_EXISTING_ID = 2;
    public static final String NAME = "Product name";
    public static final Product PRODUCT = new Product(EXISTING_ID, NAME);

    private static final Product EMPTY_PRODUCT = new Product(null, null);
    private static final Product NO_ID_PRODUCT = new Product(null, NAME);
    private static final Product NO_NAME_PRODUCT = new Product(EXISTING_ID, null);

    @Test
    @Parameters(method = "wrongInitializedProducts")
    public void givenProduct_whenAdd_thenReturnStatusCodeBadRequest(Product product) {
        ResponseEntity<Void> response = restTemplate.postForEntity(BASE_URI, product, Void.class);

        ResponseAssert.assertThat(response).hasStatusCodeBadRequest();
    }

    @Test
    public void givenProperlyInitializedProduct_whenAdd_thenReturnStatusCreatedWithLocation() {
        ResponseEntity<Void> response = restTemplate.postForEntity(BASE_URI, PRODUCT, Void.class);

        ResponseAssert.assertThat(response).hasStatusCodeCreated().hasLocation(URI.create(BASE_URI + EXISTING_ID));
    }

    @Test
    public void givenIdOfNonExistingProduct_whenGet_thenReturnStatusCodeNotFound() {
        ResponseEntity<Product> response = restTemplate.getForEntity(BASE_URI + NON_EXISTING_ID, Product.class);

        ResponseAssert.assertThat(response).hasStatusCodeNotFound();
    }

    @Test
    public void givenIdOfExistingProduct_whenGet_thenReturnProductWithStatusCodeOk() {
        ResponseEntity<Product> response = restTemplate.getForEntity(BASE_URI + EXISTING_ID, Product.class);

        ResponseAssert.assertThat(response).hasStatusCodeOk();
        ProductAssert.assertThat(response.getBody()).hasId(EXISTING_ID).hasName(NAME);
    }

    @Test
    public void givenIdOfNonExistingProduct_whenDelete_thenReturnStatusCodeNotFound() {
        ResponseEntity<Product> response = restTemplate.exchange(BASE_URI + NON_EXISTING_ID, HttpMethod.DELETE, null, Product.class);

        ResponseAssert.assertThat(response).hasStatusCodeNotFound();
    }

    @Test
    public void givenIdOfExistingProduct_whenDelete_thenReturnStatusCodeNoContent() {
        ResponseEntity<Product> response = restTemplate.exchange(BASE_URI + EXISTING_ID, HttpMethod.DELETE, null, Product.class);

        ResponseAssert.assertThat(response).hasStatusCodeNoContent();
    }

    @Test
    public void whenInvalidateCache_thenShouldReturnStatusCodeOk() {
        ResponseEntity<Void> response = restTemplate.exchange(BASE_URI + INVALIDATE_CACHE, HttpMethod.DELETE, null, Void.class);

        ResponseAssert.assertThat(response).hasStatusCodeOk();
    }

    public Object[] wrongInitializedProducts() {
        return $($(EMPTY_PRODUCT), $(NO_ID_PRODUCT), $(NO_NAME_PRODUCT));
    }


}
