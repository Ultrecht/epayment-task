package com.epayment.cache;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(HierarchicalContextRunner.class)
public class LinkedHashMapLRUCacheTest {

    private static final int MAX_ENTRIES = 2;
    private final Integer KEY_1 = 1;
    private final String VALUE_1 = "VALUE_1";

    private LRUCache<Integer, String> cache;

    @Before
    public void givenCacheWithInitialMaxEntries() {
        cache = new LinkedHashMapLRUCache<>();
        cache.setMaxEntries(MAX_ENTRIES);
    }

    public class FullCacheContext {

        private final Integer KEY_2 = 2;
        private final Integer KEY_3 = 3;
        private final String VALUE_2 = "VALUE_2";
        private final String VALUE_3 = "VALUE_3";

        @Before
        public void givenFullCache() {
            cache.put(KEY_1, VALUE_1);
            cache.put(KEY_2, VALUE_2);
        }

        @Test
        public void whenAddNewElementToFullCache_thenRemoveLastRecentlyUsedElement() throws Exception {
            cache.put(KEY_3, VALUE_3);

            Optional<String> result = cache.get(KEY_1);

            assertThat(result).isEmpty();
            assertThat(cache.size()).isEqualTo(MAX_ENTRIES);
        }

        @Test
        public void whenAddElementWithExistingKey_thenNoElementFromCacheShouldBeRemoved() throws Exception {
            cache.put(KEY_1, VALUE_1);

            assertThat(cache.size()).isEqualTo(MAX_ENTRIES);
        }

        @Test
        public void whenGetElementInCache_thenElementShouldNotBeRemovedWhenNewOneIsAdded() throws Exception {
            cache.get(KEY_1);
            cache.put(KEY_3, VALUE_3);

            Optional<String> result = cache.get(KEY_2);

            assertThat(result).isEmpty();
            assertThat(cache.size()).as("Cache size").isEqualTo(MAX_ENTRIES);
        }

        @Test
        public void whenAddElementWithExistingKey_thenElementShouldNotBeRemovedWhenNewOneIsAdded() {
            cache.put(KEY_1, VALUE_1);
            cache.put(KEY_3, VALUE_3);

            Optional<String> result = cache.get(KEY_2);

            assertThat(result).isEmpty();
            assertThat(cache.size()).as("Cache size").isEqualTo(2);
        }
    }

    public class NonEmptyCacheContext {

        @Before
        public void givenElementInCache() {
            cache.put(KEY_1, VALUE_1);
        }

        @Test
        public void whenAddElementOnTheSameKey_thenReturnTheSameElementAndNotIncreaseSize() {
            String element = cache.put(KEY_1, VALUE_1);

            assertThat(element).isEqualTo(VALUE_1);
            assertThat(cache.size()).as("Cache size").isEqualTo(1);
        }

        @Test
        public void whenGetElementByKey_thenReturnElement() throws Exception {
            Optional<String> result = cache.get(KEY_1);

            assertThat(result).isPresent().contains(VALUE_1);
        }

        @Test
        public void whenDeleteElementByKey_thenReturnDeletedElement() throws Exception {
            Optional<String> result = cache.remove(KEY_1);

            assertThat(result).isPresent().contains(VALUE_1);
        }

        @Test
        public void whenInvalidateCache_thenCacheShouldBeEmpty() throws Exception {
            cache.invalidate();

            assertThat(cache.size()).as("Cache size").isZero();
        }
    }

    public class EmptyCacheContext {

        @Test
        public void whenAddElement_thenReturnNullAndIncreaseCacheSize() throws Exception {
            String element = cache.put(KEY_1, VALUE_1);

            assertThat(element).isNull();
            assertThat(cache.size()).as("Cache size").isEqualTo(1);
        }

        @Test
        public void whenGetElementByKey_thenReturnEmptyOptional() throws Exception {
            Optional<String> result = cache.get(KEY_1);

            assertThat(result).isEmpty();
        }

        @Test
        public void whenDeleteElementByKey_thenReturnEmptyOptional() throws Exception {
            Optional<String> result = cache.remove(KEY_1);

            assertThat(result).isEmpty();
        }
    }
}
