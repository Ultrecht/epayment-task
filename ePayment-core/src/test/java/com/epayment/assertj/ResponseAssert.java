package com.epayment.assertj;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;

public class ResponseAssert extends AbstractAssert<ResponseAssert, ResponseEntity> {

    protected ResponseAssert(ResponseEntity actual) {
        super(actual, ResponseAssert.class);
    }

    public static ResponseAssert assertThat(ResponseEntity actual) {
        return new ResponseAssert(actual);
    }

    public ResponseAssert hasStatusCodeBadRequest() {
        isNotNull();
        Assertions.assertThat(actual.getStatusCode()).as("Expected status code").isEqualTo(HttpStatus.BAD_REQUEST);
        return this;
    }

    public ResponseAssert hasStatusCodeOk() {
        isNotNull();
        Assertions.assertThat(actual.getStatusCode()).as("Expected status code").isEqualTo(HttpStatus.OK);
        return this;
    }

    public ResponseAssert hasStatusCodeCreated() {
        isNotNull();
        Assertions.assertThat(actual.getStatusCode()).as("Expected status code").isEqualTo(HttpStatus.CREATED);
        return this;
    }

    public ResponseAssert hasStatusCodeNotFound() {
        isNotNull();
        Assertions.assertThat(actual.getStatusCode()).as("Expected status code").isEqualTo(HttpStatus.NOT_FOUND);
        return this;
    }

    public ResponseAssert hasStatusCodeNoContent() {
        isNotNull();
        Assertions.assertThat(actual.getStatusCode()).as("Expected status code").isEqualTo(HttpStatus.NO_CONTENT);
        return this;
    }

    public ResponseAssert hasLocation(URI location) {
        isNotNull();
        Assertions.assertThat(actual.getHeaders().getLocation()).as("Expected location").isEqualTo(location);
        return this;
    }

}

