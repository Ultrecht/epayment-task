package com.epayment.assertj;

import com.epayment.interfaces.rest.Product;
import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;

public class ProductAssert extends AbstractAssert<ProductAssert, Product> {

    protected ProductAssert(Product actual) {
        super(actual, ProductAssert.class);
    }

    public static ProductAssert assertThat(Product actual) {
        return new ProductAssert(actual);
    }

    public ProductAssert hasId(Integer id) {
        isNotNull();
        Assertions.assertThat(actual.getId()).as("Expected id").isEqualTo(id);
        return this;
    }

    public ProductAssert hasName(String name) {
        isNotNull();
        Assertions.assertThat(actual.getName()).as("Expected name").isEqualTo(name);
        return this;
    }

}
