package com.epayment.util;

import com.epayment.SpringRootConfiguration;
import org.junit.ClassRule;
import org.junit.Rule;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.web.client.RestTemplate;

@SpringApplicationConfiguration(SpringRootConfiguration.class)
@WebIntegrationTest
public abstract class SpringWebTestInit {

    @ClassRule
    public static final SpringClassRule SCR = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    protected RestTemplate restTemplate = new TestRestTemplate();

}
