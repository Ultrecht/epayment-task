package com.epayment.util;

import org.junit.Before;
import org.mockito.MockitoAnnotations;

public abstract class MockitoTestInit {

    @Before
    public void initializeMockito() {
        MockitoAnnotations.initMocks(this);
    }
}
