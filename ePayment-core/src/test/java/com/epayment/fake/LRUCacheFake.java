package com.epayment.fake;

import com.epayment.cache.LRUCache;
import com.epayment.interfaces.rest.Product;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.epayment.services.RESTProductServiceIT.EXISTING_ID;
import static com.epayment.services.RESTProductServiceIT.PRODUCT;

/**
 * This is the fake implementation of cache, only to perform integration test of REST service with net.
 * This class is annotated with {@link org.springframework.context.annotation.Primary} annotation, to have higher priority than real implementation {@link com.epayment.cache.LinkedHashMapLRUCache}.
 * Thanks to that this, fake will be used in integration test of {@link com.epayment.services.RESTProductServiceIT}.
 */
@Component
@Primary
public class LRUCacheFake implements LRUCache<Integer, Product> {

    @Override
    public Product put(Integer key, Product value) {
        return value;
    }

    @Override
    public Optional<Product> get(Integer key) {
        if (EXISTING_ID.equals(key))
            return Optional.of(PRODUCT);

        return Optional.empty();
    }

    @Override
    public Optional<Product> remove(Integer key) {
        if (EXISTING_ID.equals(key))
            return Optional.of(PRODUCT);

        return Optional.empty();
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public void invalidate() {
    }

    @Override
    public void setMaxEntries(int maxEntries) {
    }
}
