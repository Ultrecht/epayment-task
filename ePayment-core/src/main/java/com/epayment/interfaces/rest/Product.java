package com.epayment.interfaces.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;

/**
 * Probably in more advanced project, will be better to have some ACL (anti corruption layer), like for example ProductDTO.
 * This will ensure that changes in domain model will no break exposed rest api.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

    @NotNull
    private Integer id;

    @NotNull
    private String name;

    public Product() {
    }

    public Product(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
