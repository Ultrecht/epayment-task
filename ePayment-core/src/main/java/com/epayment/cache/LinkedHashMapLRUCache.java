package com.epayment.cache;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Simple LRU cache, that use {@link LinkedHashMap} as implementation. Using access order and remove eldest entry to implement LRU algorithm.
 * Implementation use {@link ReadWriteLock,} to ensure thread safety. Implementation assume that, reads will  be substantial more that writes,
 * if not simple Lock probably will be faster. {@link ReentrantReadWriteLock} is constructed as non fair.
 *
 * @param <K> type of map key
 * @param <V> type of map value
 */
@Component
public class LinkedHashMapLRUCache<K, V> implements LRUCache<K, V> {

    @Value("${maxEntries}")
    private int maxEntries;

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock read = lock.readLock();
    private final Lock write = lock.writeLock();

    private final Map<K, V> map;

    public LinkedHashMapLRUCache() {
        this.map = new LinkedHashMap<K, V>(16, 0.75f, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry eldest) {
                return super.size() > maxEntries;
            }
        };
    }


    @Override
    public V put(K key, V value) {
        write.lock();
        try {
            return map.put(key, value);
        } finally {
            write.unlock();
        }
    }

    @Override
    public Optional<V> get(K key) {
        write.lock();
        try {
            return Optional.ofNullable(map.get(key));
        } finally {
            write.unlock();
        }
    }

    @Override
    public Optional<V> remove(K key) {
        write.lock();
        try {
            return Optional.ofNullable(map.remove(key));
        } finally {
            write.unlock();
        }
    }

    @Override
    public void invalidate() {
        write.lock();
        try {
            map.clear();
        } finally {
            write.unlock();
        }
    }

    @Override
    public int size() {
        read.lock();
        try {
            return map.size();

        } finally {
            read.unlock();
        }
    }

    @Override
    public void setMaxEntries(int maxEntries) {
        write.lock();
        try {
            this.maxEntries = maxEntries;
        } finally {
            write.unlock();
        }
    }
}
