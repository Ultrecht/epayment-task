package com.epayment.cache;

import java.util.Optional;

public interface LRUCache<K, V> {

    /**
     * Associates the specified value with the specified key.
     * If the cache previously contained a mapping for the key, the old value is replaced .
     *
     * @param key the key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the previous value associated with key, or null if there was no mapping for key
     */
    V put(K key, V value);

    /**
     * Return value for specified key
     * @param key the key whose associated value is to be returned
     * @return {@link Optional} value of element associated with key, empty if there was no mapping for key
     */
    Optional<V> get(K key);

    /**
     * Remove value from cache for the specified key, if present
     * @param key the key whose mapping is to be removed
     * @return {@link Optional} value of element associated with key, empty if there was no mapping for key
     */
    Optional<V> remove(K key);

    /**
     * Return number of elements in cache.
     * @return number of elements in cache
     */
    int size();

    /**
     * Evict all elements from cache.
     */
    void invalidate();

    /**
     * Set the maximum capacity of cache, above that value last recently used elements, will be removed from cache.
     *
     * @param maxEntries maximum capacity of cache
     */
    void setMaxEntries(int maxEntries);

}
