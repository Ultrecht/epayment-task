package com.epayment.services;

import com.epayment.cache.LRUCache;
import com.epayment.interfaces.rest.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class RESTProductsService {

    private final LRUCache<Integer, Product> lruCache;

    @Autowired
    public RESTProductsService(LRUCache<Integer, Product> lruCache) {
        this.lruCache = lruCache;
    }

    @RequestMapping(value = "/product/", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Void> add(@Valid @RequestBody Product product, UriComponentsBuilder ucBuilder) {
        System.out.println("Caching product with id " + product.getId());
        lruCache.put(product.getId(), product);
        HttpHeaders headers = createHeaders(ucBuilder, product);
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    private HttpHeaders createHeaders(UriComponentsBuilder ucBuilder, Product dto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/product/{id}").buildAndExpand(dto.getId()).toUri());
        return headers;
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Product> get(@PathVariable("id") Integer key) {
        System.out.println("Fetching product with key " + key);
        Optional<Product> product = lruCache.get(key);
        return product.isPresent() ? new ResponseEntity<>(product.get(), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<Product> delete(@PathVariable("id") Integer key) {
        System.out.println("Deleting product with key " + key);
        Optional<Product> product = lruCache.remove(key);
        return product.isPresent() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/product/cache/contents", method = RequestMethod.DELETE)
    public ResponseEntity<Void> invalidate() {
        System.out.println("Cache invalidation");
        lruCache.invalidate();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
