To create runnable jar, please build application with command

mvn package

This should create uber jar file in directory ePayment-core/target, which can be run with a command

java -jar ePayment-core-1.0.0.jar

To run application without building, go to directory ePayment-core, and run command

mvn spring-boot:run


Example add product to cache

url -> http://localhost:8080/product/

Content-type: application/json

Raw data
{
  "id": 1,
  "name": "Sample product"
}

Get element from cache

url -> http://localhost:8080/product/{id}

where 1 is id of earlier added product

Remove element

url -> http://localhost:8080/product/{id}

Invalidate cache

url -> http://localhost:8080/product/cache/contents

To change max entries value, that is stored in LRU, modify property maxEntries in file config.properties
(or test-config.properties in acceptance tests), and restart application.