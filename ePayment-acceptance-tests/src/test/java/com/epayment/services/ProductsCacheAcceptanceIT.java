package com.epayment.services;

import com.epayment.SpringRootConfiguration;
import com.epayment.assertj.ProductAssert;
import com.epayment.assertj.ResponseAssert;
import com.epayment.interfaces.rest.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(SpringRootConfiguration.class)
@WebIntegrationTest
@TestPropertySource("classpath:test-config.properties")
public class ProductsCacheAcceptanceIT {

    public static final String BASE_URI = "http://localhost:8080/product/";
    public static final String INVALIDATE_CACHE = "cache/contents";

    private static final Integer ID_1 = 1;
    private static final Integer ID_2 = 2;
    private static final Integer ID_3 = 3;
    private static final String NAME_1 = "Product Name 1";
    private static final String NAME_2 = "Product Name 2";
    private static final String NAME_3 = "Product Name 3";
    private static final Product PRODUCT_ONE = new Product(ID_1, NAME_1);
    private static final Product PRODUCT_TWO = new Product(ID_2, NAME_2);
    private static final Product PRODUCT_THREE = new Product(ID_3, NAME_3);

    private RestTemplate restTemplate = new TestRestTemplate();

    @Before
    public void givenEmptyCache() {
        invalidateCache();
    }

    @Test
    public void whenAddProductToEmptyCache_thenReturnStatusCodeCreated() throws Exception {
        ResponseEntity<Void> product = restTemplate.postForEntity(BASE_URI, PRODUCT_ONE, Void.class);

        ResponseAssert.assertThat(product).hasStatusCodeCreated().hasLocation(URI.create(BASE_URI + ID_1));
    }

    @Test
    public void givenProductInCache_whenGetProduct_theReturnProductAndStatusCodeOk() throws Exception {
        URI productOneLocation = addProductToCache(PRODUCT_ONE);

        ResponseEntity<Product> productOne = getProductFromLocation(productOneLocation);

        ProductAssert.assertThat(productOne.getBody()).hasId(ID_1).hasName(NAME_1);
    }

    @Test
    public void givenProductInCache_whenDeleteProduct_thenRemoveProductAndReturnStatusNoContent() throws Exception {
        URI productOneLocation = addProductToCache(PRODUCT_ONE);

        ResponseEntity<Product> removedProductOne = deleteProductFromLocation(productOneLocation);

        ResponseAssert.assertThat(removedProductOne).hasStatusCodeNoContent();
        ResponseEntity<Product> productOne = getProductFromLocation(productOneLocation);
        ResponseAssert.assertThat(productOne).hasStatusCodeNotFound();
    }

    @Test
    public void givenEmptyCache_whenGetProduct_thenReturnStatusCodeNotFound() throws Exception {
        URI productOneLocation = URI.create(BASE_URI + ID_1);

        ResponseEntity<Product> productOne = getProductFromLocation(productOneLocation);

        ResponseAssert.assertThat(productOne).hasStatusCodeNotFound();
    }

    @Test
    public void givenEmptyCache_whenDeleteProduct_thenReturnStatusCodeNotFound() throws Exception {
        URI productOneLocation = URI.create(BASE_URI + ID_1);

        ResponseEntity<Product> productOne = deleteProductFromLocation(productOneLocation);

        ResponseAssert.assertThat(productOne).hasStatusCodeNotFound();
    }

    @Test
    public void givenThreeProducts_whenAddToCache_thenLRUProductShouldBeRemoved() throws Exception {
        URI productOneLocation = addProductToCache(PRODUCT_ONE);
        URI productTwoLocation = addProductToCache(PRODUCT_TWO);
        URI productThreeLocation = addProductToCache(PRODUCT_THREE);

        ResponseEntity<Product> productOne = getProductFromLocation(productOneLocation);
        ResponseEntity<Product> productTwo = getProductFromLocation(productTwoLocation);
        ResponseEntity<Product> productThree = getProductFromLocation(productThreeLocation);

        ResponseAssert.assertThat(productOne).hasStatusCodeNotFound();
        ResponseAssert.assertThat(productTwo).hasStatusCodeOk();
        ResponseAssert.assertThat(productThree).hasStatusCodeOk();
    }

    @Test
    public void givenFullCache_whenInvalidate_thenCacheShouldBeEmpty() throws Exception {
        URI productOneLocation = addProductToCache(PRODUCT_ONE);
        URI productTwoLocation = addProductToCache(PRODUCT_TWO);

        invalidateCache();

        ResponseEntity<Product> productOne = getProductFromLocation(productOneLocation);
        ResponseEntity<Product> productTwo = getProductFromLocation(productTwoLocation);
        ResponseAssert.assertThat(productOne).hasStatusCodeNotFound();
        ResponseAssert.assertThat(productTwo).hasStatusCodeNotFound();
    }

    @Test
    public void givenFullCache_whenChangeAccessOrderByGetProduct_thenAddingNewProductShouldRemoveLRUProduct() throws Exception {
        URI productOneLocation = addProductToCache(PRODUCT_ONE);
        URI productTwoLocation = addProductToCache(PRODUCT_TWO);
        getProductFromLocation(productOneLocation);
        URI productThreeLocation = addProductToCache(PRODUCT_THREE);

        ResponseEntity<Product> productOne = getProductFromLocation(productOneLocation);
        ResponseEntity<Product> productTwo = getProductFromLocation(productTwoLocation);
        ResponseEntity<Product> productThree = getProductFromLocation(productThreeLocation);

        ResponseAssert.assertThat(productOne).hasStatusCodeOk();
        ResponseAssert.assertThat(productTwo).hasStatusCodeNotFound();
        ResponseAssert.assertThat(productThree).hasStatusCodeOk();
    }

    @Test
    public void givenFullCache_whenChangeAccessOrderByPutProductWithTheSameID_thenAddingNewProductShouldRemoveLRUProduct() throws Exception {
        URI productOneLocation = addProductToCache(PRODUCT_ONE);
        URI productTwoLocation = addProductToCache(PRODUCT_TWO);
        addProductToCache(PRODUCT_ONE);
        URI productThreeLocation = addProductToCache(PRODUCT_THREE);

        ResponseEntity<Product> productOne = getProductFromLocation(productOneLocation);
        ResponseEntity<Product> productTwo = getProductFromLocation(productTwoLocation);
        ResponseEntity<Product> productThree = getProductFromLocation(productThreeLocation);

        ResponseAssert.assertThat(productOne).hasStatusCodeOk();
        ResponseAssert.assertThat(productTwo).hasStatusCodeNotFound();
        ResponseAssert.assertThat(productThree).hasStatusCodeOk();
    }

    private void invalidateCache() {
        restTemplate.exchange(BASE_URI + INVALIDATE_CACHE, HttpMethod.DELETE, null, Void.class);
    }

    private URI addProductToCache(Product product) {
        return restTemplate.postForEntity(BASE_URI, product, Void.class).getHeaders().getLocation();
    }

    private ResponseEntity<Product> getProductFromLocation(URI location) {
        return restTemplate.getForEntity(location, Product.class);
    }

    private ResponseEntity<Product> deleteProductFromLocation(URI location) {
        return restTemplate.exchange(location, HttpMethod.DELETE, null, Product.class);
    }
}
