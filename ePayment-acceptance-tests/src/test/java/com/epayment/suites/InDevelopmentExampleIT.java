package com.epayment.suites;


import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * This test should not pass, proving that tests from in progress suite are not fire up with acceptance tests.
 */
public class InDevelopmentExampleIT {

    @Test
    public void shouldNotPass() {
        assertTrue(false);
    }

}
