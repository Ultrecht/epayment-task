package com.epayment.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        InDevelopmentExampleIT.class
})
public class InDevelopmentSuite {
}
