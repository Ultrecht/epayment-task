package com.epayment.suites;

import com.epayment.services.ProductsCacheAcceptanceIT;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        RegressionExampleIT.class,
        ProductsCacheAcceptanceIT.class
})
public class RegressionSuite {


}
